<?php

namespace Platformsfx\Invoicable;

use Illuminate\Database\Eloquent\Model;
use Platformsfx\Invoicable\IsInvoicable\IsInvoicableTrait;

class TestModel extends Model
{
    use IsInvoicableTrait;

    protected $guarded = [];
}
