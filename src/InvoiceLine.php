<?php

namespace Platformsfx\Invoicable;

use Illuminate\Database\Eloquent\Model;
use Platformsfx\Invoicable\Invoice;

class InvoiceLine extends Model
{
    protected $guarded = [];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
